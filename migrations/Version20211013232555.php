<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211013232555 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89166D1F9C');
        $this->addSql('DROP INDEX IDX_16DB4F89166D1F9C ON picture');
        $this->addSql('ALTER TABLE picture DROP project_id');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C166D1F9C');
        $this->addSql('DROP INDEX IDX_7CC7DA2C166D1F9C ON video');
        $this->addSql('ALTER TABLE video DROP project_id');
    }

    public function isTransactional(): bool
    {
        return false;
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE picture ADD project_id INT NOT NULL');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_16DB4F89166D1F9C ON picture (project_id)');
        $this->addSql('ALTER TABLE video ADD project_id INT NOT NULL');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_7CC7DA2C166D1F9C ON video (project_id)');
    }
}
