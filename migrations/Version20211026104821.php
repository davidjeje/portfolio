<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211026104821 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE functionality ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE functionality ADD CONSTRAINT FK_F83C5F44166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_F83C5F44166D1F9C ON functionality (project_id)');
        $this->addSql('ALTER TABLE skill ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE skill ADD CONSTRAINT FK_5E3DE477166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_5E3DE477166D1F9C ON skill (project_id)');
    }

    public function isTransactional(): bool
    {
        return false;
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE functionality DROP FOREIGN KEY FK_F83C5F44166D1F9C');
        $this->addSql('DROP INDEX IDX_F83C5F44166D1F9C ON functionality');
        $this->addSql('ALTER TABLE functionality DROP project_id');
        $this->addSql('ALTER TABLE skill DROP FOREIGN KEY FK_5E3DE477166D1F9C');
        $this->addSql('DROP INDEX IDX_5E3DE477166D1F9C ON skill');
        $this->addSql('ALTER TABLE skill DROP project_id');
    }
}
