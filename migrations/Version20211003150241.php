<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211003150241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE management (id INT AUTO_INCREMENT NOT NULL, admin_id INT NOT NULL, description LONGTEXT NOT NULL, icon VARCHAR(255) NOT NULL, INDEX IDX_B4E954F642B8210 (admin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE picture (id INT AUTO_INCREMENT NOT NULL, project_id INT NOT NULL, name VARCHAR(255) NOT NULL, width NUMERIC(6, 2) DEFAULT NULL, height NUMERIC(6, 2) DEFAULT NULL, filepath VARCHAR(255) DEFAULT NULL, INDEX IDX_16DB4F89166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, admin_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, sitelink VARCHAR(255) DEFAULT NULL, repositorylink VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, startdate DATE DEFAULT NULL, enddate DATE DEFAULT NULL, INDEX IDX_2FB3D0EE642B8210 (admin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE storytelling (id INT AUTO_INCREMENT NOT NULL, admin_id INT NOT NULL, quote VARCHAR(255) NOT NULL, story LONGTEXT NOT NULL, INDEX IDX_29DE7054642B8210 (admin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video (id INT AUTO_INCREMENT NOT NULL, project_id INT NOT NULL, name VARCHAR(255) NOT NULL, width NUMERIC(6, 2) DEFAULT NULL, height NUMERIC(6, 2) DEFAULT NULL, filepath VARCHAR(255) DEFAULT NULL, INDEX IDX_7CC7DA2C166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE management ADD CONSTRAINT FK_B4E954F642B8210 FOREIGN KEY (admin_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE642B8210 FOREIGN KEY (admin_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE storytelling ADD CONSTRAINT FK_29DE7054642B8210 FOREIGN KEY (admin_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }

    public function isTransactional(): bool
    {
        return false;
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89166D1F9C');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C166D1F9C');
        $this->addSql('ALTER TABLE management DROP FOREIGN KEY FK_B4E954F642B8210');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE642B8210');
        $this->addSql('ALTER TABLE storytelling DROP FOREIGN KEY FK_29DE7054642B8210');
        $this->addSql('DROP TABLE management');
        $this->addSql('DROP TABLE picture');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE storytelling');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE video');
    }
}
