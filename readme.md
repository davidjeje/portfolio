# Portfolio
***
This site is a portfolio which presents various applications as well as the developer who realizes them.
***
***
## Status
> Project development is complete.
***
***
## The technical requirements
***
* Install PHP 7.2.5 or higher.
* Install composer which is used to install PHP packages.
* Install git to retrieve the project.
* install yarn or npm to compile webpack encore.
***
***
## Documentation
[install PHP](https://www.php.net/manual/fr/install.php)
[Symfony](https://symfony.com/)
[webpackEncore](https://symfony.com/doc/current/frontend.html#webpack-encore)
[DoctrineFixtures](https://symfony.com/doc/current/the-fast-track/en/17-tests.html#writing-functional-tests-for-controllers)
[EasyAdmin](https://symfony.com/doc/current/EasyAdminBundle/index.html)
[VichUploaderBundle](https://symfony.com/bundles/EasyAdminBundle/2.x/integration/vichuploaderbundle.html#uploading-other-types-of-files)
[LiipImagineBundle](https://symfony.com/doc/current/LiipImagineBundle/index.html)
[KnpPaginator](https://github.com/KnpLabs/KnpPaginatorBundle)
[Docker](https://docs.docker.com/)
[Minio](https://docs.min.io/docs/)
***
***
## Install the project
***
1. [Go to this remote repository](https://gitlab.com/davidjeje/portfolio)
2. __Copy this address to the command line__ with SSH: git clone git@gitlab.com:davidjeje/portfolio.git or with HTPPS: git clone https://gitlab.com/davidjeje/portfolio.git
***
***
## Bundles
***
* WebpackEncore.
* DoctrineFixtures.
* KnpPaginator.
* EasyAdmin.
* VichUploaderBundle.
* LiipImagineBundle.
***
***
## Command line
__For PHP and composer:__
* Then quickly check that PHP and Composer are available in your command prompt with php -v and composer --version.
***
__For SYMFONY:__
* Quickly launch the internal server with the symfony cli with git bash for example: symfony serve -d
* Stop the internal server with symfony cli symfony server:stop
***
__For DOCKER:__
* Launch a database with docker compose: docker-compose up -d.
***
__For data base test:__ 
* symfony console doctrine:database:create --env=test
* symfony console  doctrine:schema:create --env=test
* symfony composer req orm-fixtures --dev
* symfony console doctrine:fixtures:load --env=test
***
__For WebpackEncore:__ 
* compile asset once: yarn encore dev
* recompile assets automatically when files change: yarn encore dev --watch
* on deploy create a production build: yarn encore dev
***
* compile asset once: npm run dev
* recompile assets automatically when files change: npm run watch
* on deploy create a production build: npm run build
***
***
## Author of the project
__David Marivat__