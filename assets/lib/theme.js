// Dans le dossier easing
import './easing/easing.js';
import './easing/easing.min.js';

// Dans le dossier isotope
import './isotope/isotope.pkgd.js';
import './isotope/isotope.pkgd.min.js';

// Dans le dossier js
import './js/main.js';

// Dans le dossier owlcarousel
import './owlcarousel/owl.carousel.js';
import './owlcarousel/owl.carousel.min.js';

// Dans le dossier animate
import './animate/animate.css';
import './animate/animate.min.css';

// Dans le dossier css
import './css/style.css';

// Dans le dossier owlcarousel/asset
import './owlcarousel/assets/owl.carousel.css';
import './owlcarousel/assets/owl.carousel.min.css';
import './owlcarousel/assets/owl.theme.default.css';
import './owlcarousel/assets/owl.theme.default.min.css';
import './owlcarousel/assets/owl.theme.green.css';
import './owlcarousel/assets/owl.theme.green.min.css';