/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// any SCSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

import './lib/theme.js';

// start the Stimulus application
import './bootstrap';

//Partie JS de bootstrap
import 'bootstrap';

//Partie CSS de bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';