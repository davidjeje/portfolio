<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\OneToMany(targetEntity=Storytelling::class, mappedBy="admin", orphanRemoval=true)
     */
    private $storytellings;

    /**
     * @ORM\OneToMany(targetEntity=Management::class, mappedBy="admin", orphanRemoval=true)
     */
    private $management;

    /**
     * @ORM\OneToMany(targetEntity=Project::class, mappedBy="admin", orphanRemoval=true)
     */
    private $projects;

    public function __construct() 
    {
        $this->storytellings = new ArrayCollection();
        $this->management = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    } 

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return Collection|Storytelling[]
     */
    public function getStorytellings(): Collection
    {
        return $this->storytellings;
    }

    public function addStorytelling(Storytelling $storytelling): self
    {
        if (!$this->storytellings->contains($storytelling)) {
            $this->storytellings[] = $storytelling;
            $storytelling->setAdmin($this);
        }

        return $this;
    }

    public function removeStorytelling(Storytelling $storytelling): self
    {
        if ($this->storytellings->removeElement($storytelling)) {
            // set the owning side to null (unless already changed)
            if ($storytelling->getAdmin() === $this) {
                $storytelling->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Management[]
     */
    public function getManagement(): Collection
    {
        return $this->management;
    }

    public function addManagement(Management $management): self
    {
        if (!$this->management->contains($management)) {
            $this->management[] = $management;
            $management->setAdmin($this);
        }

        return $this;
    }

    public function removeManagement(Management $management): self
    {
        if ($this->management->removeElement($management)) {
            // set the owning side to null (unless already changed)
            if ($management->getAdmin() === $this) {
                $management->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setAdmin($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->removeElement($project)) {
            // set the owning side to null (unless already changed)
            if ($project->getAdmin() === $this) {
                $project->setAdmin(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }
} 
