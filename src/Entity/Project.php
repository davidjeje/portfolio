<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @Vich\Uploadable
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sitelink;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $repositorylink; 

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $startdate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $enddate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $admin;

    /**
     * @ORM\Column(type="text")
     */
    private $presentation;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private $width;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private $height;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $filePath;

    /**
     * @Vich\UploadableField(mapping="project_images", fileNameProperty="filePath")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity=Picture::class, mappedBy="project", cascade={"persist"})
     */
    private $picture;

    /**
     * @ORM\OneToMany(targetEntity=Skill::class, mappedBy="project")
     */
    private $skill;

    /**
     * @ORM\OneToMany(targetEntity=Functionality::class, mappedBy="project")
     */
    private $functionality;

    /**
     * @ORM\OneToMany(targetEntity=Video::class, mappedBy="project")
     */
    private $video;

    public function __construct()
    {
        $this->picture = new ArrayCollection();
        $this->skill = new ArrayCollection();
        $this->functionality = new ArrayCollection();
        $this->video = new ArrayCollection();
    }

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSitelink(): ?string
    {
        return $this->sitelink;
    }

    public function setSitelink(?string $sitelink): self
    {
        $this->sitelink = $sitelink;

        return $this;
    }

    public function getRepositorylink(): ?string
    {
        return $this->repositorylink;
    }

    public function setRepositorylink(string $repositorylink): self
    {
        $this->repositorylink = $repositorylink;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(?\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }
  
    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(?\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }
 
    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function setImageFile(File $filePath = null)
    {
        $this->imageFile = $filePath;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($filePath) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->startdate = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPicture(): Collection
    {
        return $this->picture;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->picture->contains($picture)) {
            $this->picture[] = $picture;
            $picture->setProject($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->picture->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getProject() === $this) {
                $picture->setProject(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection|Skill[]
     */
    public function getSkill(): Collection
    {
        return $this->skill;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skill->contains($skill)) {
            $this->skill[] = $skill;
            $skill->setProject($this);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skill->removeElement($skill)) {
            // set the owning side to null (unless already changed)
            if ($skill->getProject() === $this) {
                $skill->setProject(null);
            }
        }

        return $this;
    } 

    /**
     * @return Collection|Functionality[]
     */
    public function getFunctionality(): Collection
    {
        return $this->functionality;
    }

    public function addFunctionality(Functionality $functionality): self
    {
        if (!$this->functionality->contains($functionality)) {
            $this->functionality[] = $functionality;
            $functionality->setProject($this);
        }

        return $this;
    }

    public function removeFunctionality(Functionality $functionality): self
    {
        if ($this->functionality->removeElement($functionality)) {
            // set the owning side to null (unless already changed)
            if ($functionality->getProject() === $this) {
                $functionality->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideo(): Collection
    {
        return $this->video;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->video->contains($video)) {
            $this->video[] = $video;
            $video->setProject($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->video->removeElement($video)) {
            // set the owning side to null (unless already changed)
            if ($video->getProject() === $this) {
                $video->setProject(null);
            }
        }

        return $this;
    }
}
