<?php
// src/EventSubscriber/EasyAdminSubscriber
namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use App\Entity\Project;
use App\Entity\Management;
use App\Entity\Storytelling;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

/**
 * @codeCoverageIgnore 
 */
class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $security;
    private $encoder;

    public function __construct(Security $security, UserPasswordHasherInterface $encoder)
    {
        $this->security = $security;
        $this->encoder = $encoder;
    }

    public static function getSubscribedEvents()
    {
        return [
            // 'kernel.terminate'
            BeforeEntityPersistedEvent::class => [
                'setProjectMangementStoryTellingRoleAdmin',
            ],
        ];
    }

    public function setProjectMangementStoryTellingRoleAdmin(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(($entity instanceof Project)){
            $user = $this->security->getUser(); 
            $entity->setAdmin($user);
        }
        elseif(($entity instanceof Management)){
            $user = $this->security->getUser(); 
            $entity->setAdmin($user);
        }
        elseif(($entity instanceof Storytelling)){
            $user = $this->security->getUser(); 
            $entity->setAdmin($user);
        }
        elseif(($entity instanceof User)){
            $passwordEncode = $this->encoder->hashPassword($entity, $entity->getPassword());
            $entity->setPassword($passwordEncode);
            $roles = ['ROLE_ADMIN'];
            $entity->setRoles($roles);
        }
        else{
            return;
        }   
    }
} 