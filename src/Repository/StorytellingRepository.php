<?php

namespace App\Repository;

use App\Entity\Storytelling;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Storytelling|null find($id, $lockMode = null, $lockVersion = null)
 * @method Storytelling|null findOneBy(array $criteria, array $orderBy = null)
 * @method Storytelling[]    findAll()
 * @method Storytelling[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StorytellingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Storytelling::class);
    }

    // /**
    //  * @return Storytelling[] Returns an array of Storytelling objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Storytelling
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
