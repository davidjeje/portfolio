<?php

namespace App\DataFixtures;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use App\Entity\User;
use App\Entity\Functionality;
use App\Entity\Management;
use App\Entity\Picture;
use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\Storytelling;
use App\Entity\Thanks;
use App\Entity\Video;
use App\Entity\Category;
/**
 * @codeCoverageIgnore 
 */
class AppFixtures extends Fixture 
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $user = new User();

        $password = $this->encoder->hashPassword($user, 'password');

        $user->setPassword($password);
        $user->setEmail('user@test.com');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setSurname($faker->lastName());
        $user->setFirstname($faker->firstName());

        $manager->flush();

        
        $storytelling = new Storytelling();

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(true);

        $manager->persist($storytelling);

        $storytelling = new Storytelling(); 

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(false);

        $manager->persist($storytelling);

        $storytelling = new Storytelling();

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(true);

        $manager->persist($storytelling);

        $storytelling = new Storytelling();

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(false);

        $manager->persist($storytelling);

        $storytelling = new Storytelling();

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(true);

        $manager->persist($storytelling);

        $storytelling = new Storytelling();

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(false);

        $manager->persist($storytelling);

        $storytelling = new Storytelling();

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(true);

        $manager->persist($storytelling);

        $storytelling = new Storytelling();

		$storytelling->setQuote($faker->sentence());
        $storytelling->setStory($faker->paragraph());
        $storytelling->setAdmin($user);
        $storytelling->setAuthor($faker->name());
        $storytelling->setInLeft(false);

        $manager->persist($storytelling);   

        for ($i = 0; $i < 10; $i++) 
        {
            $thanks = new Thanks();

		    $thanks->setDescription($faker->sentence());

            $manager->persist($thanks);
        }

        for ($i = 0; $i < 10; $i++) 
        {
            $management = new Management();

		    $management->setDescription($faker->paragraph());
            $management->setIcon($faker->word());
            $management->setAdmin($user);
            $management->setTitle($faker->word(3, true));

            $manager->persist($management);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-1.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));          

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-2.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4'); 
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));         

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-3.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');   
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));       

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-1.jpg');

            $manager->persist($project); 

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4'); 
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));         

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-2.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4'); 
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));         

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-3.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');  
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));        

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-1.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');  
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));        

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-2.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');  
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));        

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-3.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');  
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));        

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-1.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');  
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));        

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-2.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }

            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4');
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));          

            $manager->persist($video);
        }

        for ($i = 0; $i < 1; $i++) 
        {
            $project = new Project();

		    $project->setName($faker->word());
            $project->setDescription($faker->sentence());
            $project->setSitelink('http:www//test/portfolio');
            $project->setRepositorylink('http:www//test/portfolio');
            $project->setSlug($faker->uuid());
            $project->setStartdate($faker->dateTime('d/m/Y'));
            $project->setEnddate($faker->dateTime('d/m/Y'));
            $project->setAdmin($user);
            $project->setPresentation($faker->paragraph());
            $project->setWidth(500);
            $project->setHeight(300);
            $project->setFilePath('blog-3.jpg');

            $manager->persist($project);

            for ($i = 0; $i < 4; $i++) 
            {
                $functionality = new Functionality();
    
                $functionality->setDescription($faker->paragraph());
                $functionality->setProject($project);
    
                $manager->persist($functionality); 
            }

            for ($i = 0; $i < 4; $i++) 
            {
                $skill = new Skill();
    
                $skill->setDescription($faker->sentence());
                $skill->setProject($project);
    
                $manager->persist($skill);
            }
  
            $picture = new Picture();

		    $picture->setName($faker->word());
            $picture->setWidth($faker->randomDigitNotNull());
            $picture->setHeight($faker->randomDigitNotNull());
            $picture->setFilepath('carousel-1.jpg');
            $picture->setProject($project);
            $picture->setCreateAt($faker->dateTime('d/m/Y'));
                
            $manager->persist($picture);

            $video = new Video();

		    $video->setName($faker->word());
            $video->setWidth($faker->randomDigitNotNull());
            $video->setHeight($faker->randomDigitNotNull());
            $video->setFilepath('test_2.mp4'); 
            $video->setProject($project);
            $video->setCreateAt($faker->dateTime('d/m/Y'));         

            $manager->persist($video);
            
        }

        $manager->flush();
    }
} 
 