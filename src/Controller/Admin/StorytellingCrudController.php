<?php

namespace App\Controller\Admin;

use App\Entity\Storytelling;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class StorytellingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Storytelling::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('quote'),
            TextareaField::new('story'),
            TextField::new('author'),
            BooleanField::new('inLeft'),
            AssociationField::new('admin')->hideOnForm(),
        ];
    }
}
