<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProjectCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Project::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            TextareaField::new('description'),
            TextField::new('sitelink'),
            TextField::new('repositorylink'),
            SlugField::new('slug')->setTargetFieldName('name')->hideOnIndex(),
            DateTimeField::new('startdate')->onlyOnForms(),
            DateTimeField::new('enddate')->onlyOnForms(),
            AssociationField::new('admin')->onlyOnForms(),
            TextareaField::new('presentation')->onlyOnForms(),
            IntegerField::new('width')->onlyOnForms(),
            IntegerField::new('height')->onlyOnForms(),
            //TextField::new('filePath'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('filePath')->setBasePath('/uploads/projects')->hideOnForm(), //http://127.0.0.1:9000/fichier/
        ];
    } 
}  
