<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Functionality;
use App\Entity\Management;
use App\Entity\Picture;
use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\Storytelling;
use App\Entity\Thanks;
use App\Entity\User;
use App\Entity\Video; 

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        //return parent::index();
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Portfolio');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Tableau de bord', 'fa fa-home');

        yield MenuItem::section('Lien public');
        yield MenuItem::linkToRoute('Accueil', 'fas fa-house-user', 'redirect_home_page');
        yield MenuItem::linkToLogout('Déconnexion', 'fas fa-power-off', 'app_logout');

        yield MenuItem::section('Lien Admin');
        yield MenuItem::linkToCrud('Fonctionnalités', 'fas fa-tools', Functionality::class);
        yield MenuItem::linkToCrud('Gestion d\'un projet ', 'fas fa-wrench', Management::class);
        yield MenuItem::linkToCrud('Images', 'fas fa-image', Picture::class);
        yield MenuItem::linkToCrud('Projets', 'fas fa-list-ul', Project::class);
        yield MenuItem::linkToCrud('Compétences', 'fas fa-user-graduate', Skill::class);
        yield MenuItem::linkToCrud('Histoires', 'fas fa-comments', Storytelling::class);
        yield MenuItem::linkToCrud('Remerciements', 'fas fa-thumbs-up', Thanks::class);
        yield MenuItem::linkToCrud('Membre du site', 'fas fa-user-check', User::class);
        yield MenuItem::linkToCrud('Vidéo', 'fas fa-video', Video::class);
    }
}
