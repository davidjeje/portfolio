<?php

namespace App\Controller\Admin;

use App\Entity\Thanks;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class ThanksCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Thanks::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextareaField::new('description'),
        ];
    }
}
