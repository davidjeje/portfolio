<?php

namespace App\Controller\Admin;

use App\Entity\Management;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class ManagementCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Management::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('title'),
            TextField::new('icon'),
            TextareaField::new('description'),
            AssociationField::new('admin')->hideOnForm(),
        ];
    }
}
