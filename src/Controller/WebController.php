<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\StorytellingRepository;
use App\Repository\ProjectRepository;
use App\Repository\ThanksRepository;
use App\Repository\FunctionalityRepository;
use App\Repository\SkillRepository;
use App\Repository\ManagementRepository;
use App\Repository\PictureRepository;
use App\Repository\VideoRepository;
use Knp\Component\Pager\PaginatorInterface;
use \Liip\ImagineBundle\Imagine\Cache\CacheManager;

class WebController extends AbstractController
{
    /**
     * @Route("/", name="redirect_home_page", methods={"GET"})
     */
    public function redirectHome(): Response
    {
        return $this->redirectToRoute('home_page');
    }

    /**
     * @Route("/home", name="home_page", methods={"GET"})
     */
    public function home(): Response
    {
        return $this->render('web/home.html.twig', [
            'controller_name' => 'WebController',
        ]); 
    }

    /**
     * @Route("/professional/orientation", name="professional_orientation_page", methods={"GET"})
     */
    public function professionalOrientation(StorytellingRepository $storytellingRepository, ThanksRepository $thanksRepository): Response
    {
        return $this->render('web/professionalOrientation.html.twig', [
            'controller_name' => 'WebController', 
            'storytellings' => $storytellingRepository->findAll(),
            'thanks' => $thanksRepository->findAll(),
        ]);
    }  
  
    /**
     * @Route("/my/projects", name="my_projects_page", methods={"GET"})
     */
    public function myProjects(ProjectRepository $projectRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $projects = $paginator->paginate($projectRepository->findBy([], ['id' => 'DESC']), $request->query->getInt('page', 1), 3);
        //dd($projects);

        return $this->render('web/myProjects.html.twig', [
            'controller_name' => 'WebController', 
            'projects' => $projects,
        ]);
    } 
    
    /**
     * @Route("/project/details/{slug}", name="project_details_page", methods={"GET"})
     */
    public function projectDetails(ProjectRepository $projectRepository, $slug, FunctionalityRepository $featureRepository, SkillRepository $skillRepository, PictureRepository $pictureRepository, VideoRepository $videoRepository): Response
    {
        $project = $projectRepository->findOneBy(['slug' => $slug]);
        $picture = $pictureRepository->findOneBy(['project' => $project->getId()]);
        $skill = $skillRepository->findBy(['project' => $project->getId()]);
        $functinality = $featureRepository->findBy(['project' => $project->getId()]);
        $video = $videoRepository->findOneBy(['project' => $project->getId()]);
        
        if(isset($picture) or !empty($picture) or isset($skill) && !empty($skill) or isset($functinality) && !empty($functinality) or isset($video) && !empty($video)){
            
            return $this->render('web/projectDetails.html.twig', [
                'controller_name' => 'WebController', 
                'project' => $project,
                'features' => $functinality,
                'skills' => $skill,
                'picture' => $picture,
                'video' => $video,
            ]);
        }
        else{
            return $this->render('web/projectDetails.html.twig', [
                'controller_name' => 'WebController', 
                'project' => $project,
                'features' => null,
                'skills' => null,
                'picture' => null,
                'video' => null,
            ]);
        }
        
    }

    /**
     * @Route("/project/management", name="project_management_page", methods={"GET"})
     */
    public function projectManagement(ManagementRepository $managementRepository): Response
    {
        return $this->render('web/management.html.twig', [
            'controller_name' => 'WebController', 
            'managements' => $managementRepository->findAll(),
        ]);
    }

    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"}, methods={"GET"})
     */
    public function sitemapXml(Request $request, ProjectRepository $projectRepository): Response
    {
        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];

        $urls[] = ['loc' => $this->generateUrl('redirect_home_page')];
        $urls[] = ['loc' => $this->generateUrl('home_page')];
        $urls[] = ['loc' => $this->generateUrl('professional_orientation_page')];
        $urls[] = ['loc' => $this->generateUrl('my_projects_page')];
        $urls[] = ['loc' => $this->generateUrl('project_management_page')];

        foreach($projectRepository->findAll() as $project){
            $urls[] = ['loc' => $this->generateUrl('project_details_page', ['slug' => $project->getSlug()] )];
        }
        
        $response = new Response($this->renderView('web/sitemapXml.html.twig', ['urls' => $urls, 'hostname' => $hostname,]), 200);
        $response->headers->set('Content-type', 'text/xml');

        return $response;
    }  
}
 