<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Project;
use App\Entity\User;

class ProjectUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setSurname('test');
		$user->setFirstname('unit');

        $date = new \dateTime('10/10/21');
		$project = new Project();

		$project->setName('project');
		$project->setDescription('test of project');
		$project->setSitelink('public/image/testUnit/project');
		$project->setRepositorylink('gitlab/test/project');
		$project->setSlug('123456');
        $project->setStartdate($date);
        $project->setEnddate($date);
		$project->setPresentation('presentation of project');
		$project->setAdmin($user);
		$project->setWidth('width');
		$project->setHeight('height');
		$project->setFilePath('filePath');
		
		$this->assertTrue($project->getName() === 'project');
		$this->assertTrue($project->getDescription() === 'test of project');
		$this->assertTrue($project->getSitelink() === 'public/image/testUnit/project');
		$this->assertTrue($project->getRepositorylink() === 'gitlab/test/project');
		$this->assertTrue($project->getSlug() === '123456');
        $this->assertTrue($project->getStartdate() === $date);
        $this->assertTrue($project->getEnddate() === $date);
		$this->assertTrue($project->getPresentation() === 'presentation of project');
		$this->assertTrue($project->getAdmin() === $user);
		$this->assertTrue($project->getWidth() === 'width');
		$this->assertTrue($project->getHeight() === 'height');
		$this->assertTrue($project->getFilePath() === 'filePath');		
	}

    public function testIsFalse()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setSurname('test');
		$user->setFirstname('unit');

		$date = new \dateTime('10/10/21');
        $date2 = new \dateTime('11/11/21');
		$project = new Project();

		$project->setName('project');
		$project->setDescription('test of project');
		$project->setSitelink('public/image/testUnit/project');
		$project->setRepositorylink('gitlab/test/project');
		$project->setSlug('123456');
        $project->setStartdate($date);
        $project->setEnddate($date);
		$project->setPresentation('presentation of project');
		$project->setAdmin($user);
		$project->setWidth('width');
		$project->setHeight('height');
		$project->setFilePath('filePath');
		
		$this->assertFalse($project->getName() === 'project2');
		$this->assertFalse($project->getDescription() === 'test of project2');
		$this->assertFalse($project->getSitelink() === 'public/image/testUnit/project2');
		$this->assertFalse($project->getRepositorylink() === 'gitlab/test/project2');
		$this->assertFalse($project->getSlug() === '1234567');
        $this->assertFalse($project->getStartdate() === $date2);
        $this->assertFalse($project->getEnddate() === $date2);
		$this->assertFalse($project->getPresentation() === 'presentation of project 2');
		$this->assertFalse($project->getAdmin() === '$user');
		$this->assertFalse($project->getWidth() === 'width2');
		$this->assertFalse($project->getHeight() === 'height2');
		$this->assertFalse($project->getFilePath() === 'filePath2');		
	}

    public function testIsEmpty()
	{
		$project = new Project();
		
		$this->assertEmpty($project->getName());
		$this->assertEmpty($project->getDescription());
		$this->assertEmpty($project->getSitelink());
		$this->assertEmpty($project->getRepositorylink());
		$this->assertEmpty($project->getSlug());
        $this->assertEmpty($project->getStartdate());
        $this->assertEmpty($project->getEnddate());	
		$this->assertEmpty($project->getPresentation());
		$this->assertEmpty($project->getAdmin());
		$this->assertEmpty($project->getWidth());
		$this->assertEmpty($project->getHeight());
		$this->assertEmpty($project->getFilePath());			
	}
}
