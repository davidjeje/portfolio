<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Thanks;

class ThanksUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$thanks = new Thanks();

		$thanks->setdescription('Test management');	
		
		$this->assertTrue($thanks->getDescription() === 'Test management');				
	}

    public function testIsFalse()
	{
		$thanks = new Thanks();

		$thanks->setdescription('Test management');	
		
		$this->assertFalse($thanks->getDescription() === 'Test management2');		
	}

    public function testIsEmpty()
	{
		$thanks = new Thanks();
		
		$this->assertEmpty($thanks->getDescription());
		$this->assertEmpty($thanks->getId());		
	}
}
