<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Storytelling;
use App\Entity\User;

class StorytellingUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setSurname('test');
		$user->setFirstname('unit');
		
		$storytelling = new Storytelling(); 

		$storytelling->setQuote('test');
		$storytelling->setStory('storytelling test');
		$storytelling->setAuthor('story');
		$storytelling->setInLeft('story');
		$storytelling->setAdmin($user);
		
		
		$this->assertTrue($storytelling->getQuote() === 'test');
		$this->assertTrue($storytelling->getStory() === 'storytelling test');
		$this->assertTrue($storytelling->getAuthor() === 'story');
		$this->assertTrue($storytelling->getAdmin() === $user);
				
	}

    public function testIsFalse()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setSurname('test');
		$user->setFirstname('unit');

		$storytelling = new Storytelling();

		$storytelling->setQuote('test');
		$storytelling->setStory('storytelling test');
		$storytelling->setAuthor('story');
		$storytelling->setInLeft('story');
		$storytelling->setAdmin($user);
		
		
		$this->assertFalse($storytelling->getQuote() === 'test2');
		$this->assertFalse($storytelling->getStory() === 'storytelling test2');	
		$this->assertFalse($storytelling->getAuthor() === 'story2');
		$this->assertFalse($storytelling->getInLeft() === 'story2');
		$this->assertFalse($storytelling->getAdmin() === '$user');	
	}

    public function testIsEmpty()
	{
		$storytelling = new Storytelling();
		
		$this->assertEmpty($storytelling->getQuote());
		$this->assertEmpty($storytelling->getStory());
		$this->assertEmpty($storytelling->getAuthor());
		$this->assertEmpty($storytelling->getId());	
		$this->assertEmpty($storytelling->getInLeft());	
		$this->assertEmpty($storytelling->getAdmin());				
	}
}
