<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Management;
use App\Entity\User;

class ManagementUnitTest extends TestCase
{
    public function testIsTrue() 
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setSurname('test');
		$user->setFirstname('unit');

		$management = new Management();

		$management->setTitle('Management');
		$management->setdescription('Test management');
        $management->setIcon('test/icon');
		$management->setAdmin($user);	
		
		$this->assertTrue($management->getTitle() === 'Management');
		$this->assertTrue($management->getDescription() === 'Test management');
        $this->assertTrue($management->getIcon() === 'test/icon');
		$this->assertTrue($management->getAdmin() === $user);				
	}

    public function testIsFalse()
	{
		$user = new User();
		$user->setEmail('test@gmail2.com');
		$user->setRoles(['ROLE_ADMIN']);
		$user->setPassword('testUnit2');
		$user->setSurname('test2');
		$user->setFirstname('unit2');

		$management = new Management();

		$management->setTitle('Management');
		$management->setdescription('Test management');
        $management->setIcon('test/icon');
		$management->setAdmin($user);	
		
		$this->assertFalse($management->getTitle() === 'Management 2');
		$this->assertFalse($management->getDescription() === 'Test management 2');
        $this->assertFalse($management->getIcon() === 'test/icon/2');	
		$this->assertFalse($management->getAdmin() === '$user');	
	}

    public function testIsEmpty()
	{
		$management = new Management();
		
		$this->assertEmpty($management->getTitle());
		$this->assertEmpty($management->getDescription());
        $this->assertEmpty($management->getIcon());
		$this->assertEmpty($management->getId());
		$this->assertEmpty($management->getAdmin());			
	}
}
