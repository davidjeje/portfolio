<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Video;

class VideoUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$video = new Video();

		$video->setName('test video');
		$video->setWidth(20.20);
        $video->setHeight(20.20);
		$video->setFilepath('public/image/video/test');
		
		
		$this->assertTrue($video->getName() === 'test video');
		$this->assertTrue($video->getWidth() == 20.20);
        $this->assertTrue($video->getHeight() == 20.20);
		$this->assertTrue($video->getFilepath() === 'public/image/video/test');				
	}

    public function testIsFalse()
	{
		$video = new Video();

		$video->setName('test video');
		$video->setWidth(20.20);
        $video->setHeight(20.20);
		$video->setFilepath('public/image/storytelling/test');
		
		
		$this->assertFalse($video->getName() === 'test video2');
        $this->assertFalse($video->getWidth() === 20.30);
		$this->assertFalse($video->getHeight() === 20.30);
		$this->assertFalse($video->getFilepath() === 'public/image/video/test2');		
	}

    public function testIsEmpty()
	{
		$video = new Video();
		
		$this->assertEmpty($video->getName());
        $this->assertEmpty($video->getWidth());
		$this->assertEmpty($video->getHeight());
		$this->assertEmpty($video->getFilepath());
		$this->assertEmpty($video->getId());			
	}
}
