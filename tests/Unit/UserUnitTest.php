<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\User;
use App\Entity\Storytelling;
use App\Entity\Management;
use App\Entity\Project;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setSurname('test');
		$user->setFirstname('unit');
		
		$this->assertTrue($user->getEmail() === 'test@gmail.com');
		$this->assertTrue($user->getUsername() === 'test@gmail.com');
		$this->assertTrue($user->getUserIdentifier() === 'test@gmail.com');
		$this->assertTrue($user->getRoles() === ['ROLE_USER']);
		$this->assertTrue($user->getPassword() === 'testUnit');
		$this->assertTrue($user->getSurname() === 'test');
		$this->assertTrue($user->getFirstname() === 'unit');
	}
 
    public function testIsFalse()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setSurname('test');
		$user->setFirstname('unit');
		
		$this->assertFalse($user->getEmail() === 'test1@gmail.com');
		$this->assertFalse($user->getRoles() === ['ROLE_ADMIN']);
		$this->assertFalse($user->getPassword() === 'testUnit2');
		$this->assertFalse($user->getSurname() === 'test2');
		$this->assertFalse($user->getFirstname() === 'unit2');		
	}

    public function testIsEmpty()
	{
		$user = new User();
		
		$this->assertEmpty($user->getEmail());
		$this->assertNotEmpty($user->getRoles());
		$this->assertEmpty($user->getSurname());
		$this->assertEmpty($user->getFirstname());
		$this->assertEmpty($user->getId());
		$this->assertEmpty($user->getSalt());		
	}

	public function testAddGetRemoveStorytelling()
	{
		$storytelling = new Storytelling(); 
		$user = new User();
		
		$this->assertEmpty($user->getStorytellings());

		$user->addStorytelling($storytelling);
		$this->assertContains($storytelling,$user->getStorytellings());
		
		$user->removeStorytelling($storytelling);
		$this->assertEmpty($user->getStorytellings());
	}

	public function testAddGetRemoveManagement()
	{
		$management = new Management(); 
		$user = new User();
		
		$this->assertEmpty($user->getManagement());

		$user->addManagement($management);
		$this->assertContains($management,$user->getManagement());
		
		$user->removeManagement($management);
		$this->assertEmpty($user->getManagement());
	}

	public function testAddGetRemoveProject()
	{
		$project = new Project(); 
		$user = new User();
		
		$this->assertEmpty($user->getProjects());

		$user->addProject($project);
		$this->assertContains($project,$user->getProjects());
		
		$user->removeProject($project);
		$this->assertEmpty($user->getProjects());
	}	
} 
