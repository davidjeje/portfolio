<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Picture;

class PictureUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$picture = new Picture();

		$picture->setName('test picture');
		$picture->setWidth(20.20);
        $picture->setHeight(20.20);
		$picture->setFilepath('public/image/storytelling/test');
		
		
		$this->assertTrue($picture->getName() === 'test picture');
		$this->assertTrue($picture->getWidth() == 20.20);
        $this->assertTrue($picture->getHeight() == 20.20);
		$this->assertTrue($picture->getFilepath() === 'public/image/storytelling/test');				
	}

    public function testIsFalse()
	{
		$picture = new Picture();

		$picture->setName('test picture');
		$picture->setWidth(20.20);
        $picture->setHeight(20.20);
		$picture->setFilepath('public/image/storytelling/test');
		
		
		$this->assertFalse($picture->getName() === 'test picture2');
        $this->assertFalse($picture->getWidth() === 20.30);
		$this->assertFalse($picture->getHeight() === 20.30);
		$this->assertFalse($picture->getFilepath() === 'public/image/storytelling/test2');		
	}

    public function testIsEmpty()
	{
		$picture = new Picture();
		
		$this->assertEmpty($picture->getName());
        $this->assertEmpty($picture->getWidth());
		$this->assertEmpty($picture->getHeight());
		$this->assertEmpty($picture->getFilepath());
		$this->assertEmpty($picture->getId());			
	}
}
