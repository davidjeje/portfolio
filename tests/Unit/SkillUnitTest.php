<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Skill;

class SkillUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$skill = new Skill();

		$skill->setdescription('Test management');	
		
		$this->assertTrue($skill->getDescription() === 'Test management');				
	}

    public function testIsFalse()
	{
		$skill = new Skill();

		$skill->setdescription('Test management');	
		
		$this->assertFalse($skill->getDescription() === 'Test management2');		
	}

    public function testIsEmpty()
	{
		$skill = new Skill();
		
		$this->assertEmpty($skill->getDescription());
		$this->assertEmpty($skill->getId());		
	}
}
