<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Functionality;

class FunctionalityUnitTest extends TestCase
{
    public function testIsTrue()
	{
		$functionality = new Functionality();

		$functionality->setdescription('Test management');	
		
		$this->assertTrue($functionality->getDescription() === 'Test management');				
	}

    public function testIsFalse()
	{
		$functionality = new Functionality();

		$functionality->setdescription('Test management');	
		
		$this->assertFalse($functionality->getDescription() === 'Test management 2');		
	}

    public function testIsEmpty()
	{
		$functionality = new Functionality();
		
		$this->assertEmpty($functionality->getDescription());
		$this->assertEmpty($functionality->getId());		
	}
}
