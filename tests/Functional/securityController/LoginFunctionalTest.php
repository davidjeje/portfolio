<?php
  
namespace App\Tests\Functional\SecurityController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginFunctionalTest extends WebTestCase
{
    public function testShouldDisplayLogin() 
    {
        $client = static::createClient();
        $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'David Marivat');
        $this->assertSelectorTextContains('p', 'david.marivat-larose@orange.fr');
    }

    public function testFormLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $buttonCrawler = $crawler->selectButton('se connecter');
        $form = $buttonCrawler->form();

        $form = $buttonCrawler->form([
            'email' => 'user@test.com',
            'password' => 'password',
        ]);

        $client->submit($form);
        //$client->followRedirect();
        $client->request('GET', '/login');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('div', 'You are logged in as user@test.com');
        //$this->assertSelectorTextContains('h2', 'David Marivat');        
    } 
} 