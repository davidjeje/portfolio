<?php

namespace App\Tests\Functional;

//use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProjectDetailsFunctionalTest extends WebTestCase
{
    public function testDisplayProjectDetailsPage(): void
    {
        //$client = static::createPantherClient();
        $client = static::createClient();
        $crawler = $client->request('GET', '/project/details/e3440a83-a132-3aca-9b30-3b05c1eb01dc');

        $this->assertSelectorTextContains('h2', 'David Marivat');
        $this->assertResponseIsSuccessful();
    }
}
