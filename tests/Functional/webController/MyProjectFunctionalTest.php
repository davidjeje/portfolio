<?php

namespace App\Tests\Functional;

//use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MyProjectsFunctionalTest extends WebTestCase
{
    public function testDisplayMyProjectsPage(): void
    {
        //$client = static::createPantherClient();
        $client = static::createClient();
        $crawler = $client->request('GET', '/my/projects');

        $this->assertSelectorTextContains('h2', 'David Marivat');
        $this->assertResponseIsSuccessful();
    }
}
