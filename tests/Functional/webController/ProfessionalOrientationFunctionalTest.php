<?php

namespace App\Tests\Functional;

//use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfessionalOrientationFunctionalTest extends WebTestCase
{
    public function testDisplayProfessionalOrientationPage(): void
    {
        //$client = static::createPantherClient();
        $client = static::createClient();
        $crawler = $client->request('GET', '/professional/orientation');

        $this->assertSelectorTextContains('h2', 'David Marivat');
        $this->assertResponseIsSuccessful();
    }
}
