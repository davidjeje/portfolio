<?php

namespace App\Tests\Functional;

//use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ManagementFunctionalTest extends WebTestCase
{
    public function testDisplayManagementPage(): void
    {
        //$client = static::createPantherClient();
        $client = static::createClient();
        $crawler = $client->request('GET', '/project/management');

        $this->assertSelectorTextContains('h2', 'David Marivat');
        $this->assertResponseIsSuccessful();
    }
}
 