<?php

namespace App\Tests\Functional;

//use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeFunctionalTest extends WebTestCase
{
    public function testDisplayHomePage(): void
    {
        //$client = static::createPantherClient();
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $client->followRedirect();

        $this->assertSelectorTextContains('h2', 'David Marivat');
        $this->assertResponseIsSuccessful();
    }
}
